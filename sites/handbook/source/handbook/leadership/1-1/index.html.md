---
layout: handbook-page-toc
title: "1-1"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Maintaining an effective and efficient agenda is important to get the best out of the 1-1 meetings you have with your team members. Furthermore this page will take you through other tips and tricks to conduct an effective 1-1 meeting. 

## The 1-1 Agenda
1. Make sure you use a consistent agenda format from week to week 
1. Both parties add items to the agenda. Preferably, the majority added by the team member. If the manager puts more than half of the items on the agenda this is an indication that something is wrong.
1. Reference the [suggested format for leadership](/handbook/leadership/1-1/suggested-agenda-format) as necessary.

## Conducting a 1-1 

We recorded a training about 1-1s which you can find here:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/KUxxjGJv1dQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


1. Meetings should normally be 25 minutes long once a week. If needed increase the frequency. Avoid scheduling 1-1s less frequently than weekly - it's better to have a short meeting with little to discuss than to go too long without an opportunity to communicate face-to-face.
1. Create a Google doc as the [agenda](#the-1-1-agenda) and set the sharing settings exclusively between you and the team member. This should not be a public document because performance feedback should be as private as possible.
1. From [High Output Management](https://getlighthouse.com/blog/high-output-management/) (edited sightly for language):
    1. “A key point about a one-on-one: it should be regarded as the reports’s meeting, with its agenda and tone set by them ... issues that preoccupy and nag the individual contributor.”
    1. How often you should have 1-1 meetings: "The answer is the job- or task-relevant maturity of each of your individual contributors. In other words, how much experience does a given report have with the specific task at hand?...the most effective management style instance varies from very close to very loose supervision as a report’s task maturity increases."
1. Bill Campbell, executive coach to top executives at Google, had a [suggested approach to the 1-1](https://alearningaday.com/2015/11/29/bill-campbell-style-1-1/). Instead of leaving the conversation open, he required both the manager and the team member to bring a list of 5 things to discuss. At the start of the meeting, they would match lists and talk about whatever is on both lists first. After that, they would spend time on 4 topics – performance on job requirements, relationships with peer teams, leadership and innovation.
1. If you have negative or positive feedback give it right away rather than waiting for the 1-1. However, make sure bi-directional feedback is given _at least_ as often as the 1-1 meeting. The face to face 1-1 is also useful for feedback that the person may be especially sensitive too, or is being given for the second time and needs to be taken more seriously.
1. It’s important not to push times of the 1-1’s for “more important” tasks. Book them and ensure you always are on time. Similarly, canceling a 1-1 should be a last resort. The exception to this is recruiting interviews. If participating in an interview schedule, reschedule the 1:1 with as much advance notice as possible, but prioritize the interview.
1. One communication style does not fit all. Some need very direct feedback. Others work better with FYI style information to then come to the conclusion you want them to come to on their own. Others work well with clear goals, but without a clear prescription as to how to reach the goal. Great managers can adapt their style to the report.
1. It is common to start with a bit of small talk. You can also consider starting with asking how are you as a person?
1. The end of the meeting is a good time to ask questions people might be hesitant to answer. [People will often reveal their important information at the end of a conversation.](https://leadingstrategicinitiatives.com/2011/07/19/use-the-columbo-question-to-get-strategic-information/) Use this to get information about things that are bothering them about other people in the company, including you as a manager. Ask a question like 'How can I make your life better?' or 'How’s the team and the work with other people?'. Don't put this question on the agenda in advance. Since it is the end of the meeting it might be needed to add it to the agenda to discuss in the next meeting.
1. Setting and managing expectations is maybe your most important task. Both the managers expectations of the work done by the report and the reports expectations about the work and company.
1. Reports tend to assume that they must do everything added by the manager, make it clear that they can push back (not a good idea, not worth the time), and redirect (please handle it directly with this person, can you arrange for this). You want to prevent the agenda piling up. The manager will put items on the agenda because it is in the functional area of the report and the manager doesn't want to bypass them. But in case the manager cares about something and the report doesn't it might be preferable for the manager (or an executive assistant) to do the work.
1. If you have few items on the agenda from the report try the following questions:
  - Is there anything that needs clarity?
  - What are you most proud of/excited about?
  - What can I (your manager) improve on?
  - Anything you think important that happened since the last 1-1
  - What are potential troubles you see for the team
  - What are potential troubles you see for the company
  - What went well last week?
  - What went not so well last week?
  - Is there anything I can do to help?
  - Anything non-work related worth mentioning?
  - When x happened, what will you do differently next time?
  - Have you identified any career development opportunities that I can help you with? For example: [Internship for Learning](/handbook/people-group/promotions-transfers/#internship-for-learning)
  - Other great questions to consider can be found in the [Culture Amp Blog: 24 great one-on-one meeting questions](https://www.cultureamp.com/blog/great-one-on-one-meeting-questions/) or in 15Five's [The Great eBook of Employee Questions](https://www.15five.com/ebook/employee-questions/).

## Career Development Discussion at the 1-1

We recorded a training about career mapping which you can find here:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/YoZH5Hhygc4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

These discussions should take place once a month and after the [360 Feedback](/handbook/people-group/360-feedback/) meeting has taken place. As a manager of people you play a crucial part in developing careers for your reports. This is for them but you should be able to support this process and help them achieve their goals. This is a join collaboration so prior to the meeting think about what questions to ask, specifically identifying **competencies(c)**,**skill gaps(sg)** and **career objectives (co)**. Adrienne Smith who wrote [increase employee retention with career pathing](https://www.geteverwise.com/human-resources/increase-employee-retention-with-career-pathing/) suggests the following:

1. Which projects are you most proud of that you’ve finished here? (c)
1. What is your favorite part of your job? (c)
1. Which projects have you struggled with most in this role? (sg)
1. What’s your least favorite part of your day-to-day? (sg)
1. When do you ask for help most often? (sg)
1. What parts of your role do you want to do more of? Less of? (co)
1. What don’t you do in your current role that you’d like to? (co)
1. What would you like to learn next? (co)

Once you have established the goals you can then create a [career development plan](/handbook/people-group/learning-and-development/#career-mapping-and-development)

### Key Points

1. **Actively Listen**. Self-assessment is difficult and people often overestimate or underestimate their skillset. Don’t be quick to discount their assessment. Look for common ground and focus on understanding their overall goals.

1. **Control**. Maintain control of the conversation to ensure it stays on track. The focus should be on their current skillset and abilities and how to cultivate those for a career path within the company.

1. **Adaptation**. Adapting your approach to different personality types is key. People that overestimate their skillsets should be given specifics on where they do, and don’t, meet expectations. They may need areas of failing to be pointed out more plainly (but always caringly). Those that underestimate their skillset may need more emphasis on what they are doing right as they tend to focus on the negative. Also, not everyone wants to advance. Some are very happy in the role they are in and want to stay there. That should also be supported. Learn more in our [Learning & Development Handbook](/handbook/people-group/learning-and-development/).

## Transitioning 1-1s

When taking over management of an existing team member, consider the following guidelines to ensure the team member experiences a psychologically safe transition.

### Manager Transition Meeting
In an effort to be [transparent](/handbook/values/#transparency), managers should consider ensuring that all of the information provided to the future manager by the current manager is done in full view of the team member. This might not work for all team members. You should ask whether the team member will feel comfortable having this conversation openly, and respect their boundaries. When choosing to remain transparent, schedule a transition meeting and follow the [Manager Transition Meeting Agenda](#manager-transition-meeting-agenda-example). 

### 1-1-1 Transition Schedule
The following schedule can ensure a smooth transition:
1. Current Manager: Create a 1-1-1 and invite the team member and the future manager
1. Get approval from the team member to share the existing 1-1 document with the future manager. Or, start a new 1-1 document and share it with the team member and the future manager
1. Team Member: If a new 1-1 document was started, transfer any relevant content
1. Group: Continue 1-1-1s for up to a month (if needed)
1. New Manager: Remove the former manager from calendar invites and the 1-1 document

Abrupt 1-1 transitions can threaten a team member's sense of psychological safety and career stability. If this is a concern, consider scheduling follow-on 1-1-1 transition meetings.

### Manager Transition Meeting Agenda (example)

#### Attendees
- Team Member:
- Current Manager:
- Future Manager:

#### Introduction
TEAM MEMBER, in the coming weeks I will transition management support to FUTURE MANAGER. This is an opportunity for you to comment, agree, and disagree with how your performance, feedback, professional goals, growth areas, and 1:1 document content is shared with FUTURE MANAGER. We want to ensure you have a complete picture of the information we share. The goal is to make sure we’re transparent in the transition of your career, how you want to grow, and how I can support you.

We understand that this meeting format may feel a bit awkward as we will openly discuss your work performance. This is all in the service of the goal to fully support you.

#### Agenda
- Introductions and reason why this manager transition is happening.
- Review most recent performance evaluations, compa group worksheets, OKRs, and goals.
- Share any additional feedback since recent reviews.
- Are there any follow-on 1-1-1 transition meetings needed?

## Shadowed 1-1

Shadowing a 1-1 is a scenario in which a GitLab team member attends the 1-1 of their manager and their manager's manager. The intent of this model to provide trust through transparency, build connections between team members and leaders who they may not otherwise interact with regularly, and open opportunities for collaboration be providing a broader knowledge to team members. The shadowed 1-1 is exemplified by Sid by having the CEO Shadows attend 1-1s with his direct reports.

### Agenda

Portions of the agenda will be suitable for sharing and others will not. Care should be taken to separate items that could breach our communication guidelines or potentially reveal confidential information. A suggestion on how to handle this would either be to not share the agenda with the shadow(s) or have a separate agenda for private items, these are just suggestions. 

### Attendees

Attendance could be limited to direct reports but could also include other people. A suggestion might be to have a sign up for attendance and, in order to maintain the relationship and support expected to arise from 1-1 meetings, alternate shadow weeks with non-shadowed weeks. Suggest having a separate zoom link for shadowed 1-1s.

### Example

An engineering manager decides to open up their 1-1 meetings to their team. The engineering manager checks with their manager that this is acceptable and determines a plan for review. When a shadow will be present, this should be indicated clearly on the agenda. Utilising a separate zoom link, all 3 people join and walk through the agenda. Participation level of the team member is up to the regular participants of the 1-1 but, consider encouraging engagement. At a point where sensitive topics need to be discussed, the team member will be asked to leave.

