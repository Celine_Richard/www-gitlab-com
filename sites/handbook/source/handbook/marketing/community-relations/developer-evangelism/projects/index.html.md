---
layout: handbook-page-toc
title: "Projects"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## everyonecancontribute.com

Maintainer: [Michael Friedrich](/company/team/#dnsmichi)

[everyonecancontribute.com](https://everyonecancontribute.com) serves as the main website for a community formed around the `#everyonecancontribute Kaeffchen`. This is a community coffee chat in German with growing numbers of community members joining.

* The website is built using [GitLab pages](https://about.gitlab.com/stages-devops-lifecycle/pages/).
* Organization happens in the GitLab group [everyonecancontribute](https://gitlab.com/groups/everyonecancontribute/-/issues).
* Communication via [Gitter](https://gitter.im/everyonecancontribute).

### `#everyonecancontribute Kaeffchen`

* Announced via the website and social media.
* Using GitLab's Zoom and YouTube streaming features.
* GitLab Unfiltered [YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko8J8V5V794CXZUZ-DLxccI).
* Organisation [kickoff](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2482) and [current project](https://gitlab.com/everyonecancontribute/general/-/issues?label_name%5B%5D=kaeffchen).

Spoken language is German since this targets the EMEA/DACH region and local communities. Written communication is English for information and inspiration sharing.

## everyonecancontribute.dev

[everyonecancontribute.dev](https://everyonecancontribute.dev) hosts a demo page with funny animations featuring the Tanuki, Clippy, and more. It is deployed in a container environment and provides a Prometheus node exporter for monitoring demos and talks. Michael created the website for the job application presentation panel at GitLab.

## Developer Evangelism Dashboard

Maintainer: [Abubakar Siddiq Ango](/company/team/#abuango)

Our [custom dashboard](https://gitlab-com.gitlab.io/marketing/corporate_marketing/technical-evangelism/code/te-dashboard/) is built using [GitLab pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) and presents an overview of Developer Evangelism issues. The requests are synced in a specified interval.

Project: [TE Dashboard](https://gitlab.com/gitlab-com/marketing/corporate_marketing/technical-evangelism/code/te-dashboard)

## Technical Evangelism Bot

Maintainer: [Abubakar Siddiq Ango](/company/team/#abuango)

Similar to the triage bot, this bot aims to automate TE Tasks such as:

* Pull social media metrics from team members into a defined spreadsheet
* Create release evangelism issues for team members on the 15th every month.
* Generate an issue letter (created, closed, open CFPs) on every Monday.

Project: [TE Bot](https://gitlab.com/gitlab-com/marketing/corporate_marketing/technical-evangelism/code/te-bot)

## Lab Work

Maintainer: [Brendan O'Leary](/company/team/#brendan)

[labwork.dev](https://labwork.dev/) is a collection of applications made to show off exciting ideas and development challenges.

* Link Shortener
* [15th Anniversary of Git](https://git15.labwork.dev/)

Project: [Lab Work](https://gitlab.com/brendan/labwork)

## Our Work Environments

* [Brendan's dotfiles](https://gitlab.com/brendan/dotfiles)
* [Michael's dotfiles](https://gitlab.com/dnsmichi/dotfiles) covered in [this blog post](https://about.gitlab.com/blog/2020/04/17/dotfiles-document-and-automate-your-macbook-setup/)
