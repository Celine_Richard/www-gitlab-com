---
layout: handbook-page-toc
title: "Account Based Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is account based marketing?
Account-based marketing is a strategic approach to marketing based on account awareness in which an organization considers and communicates with individual prospect or customer accounts as markets of one.  Through a close strategic alignment between sales and marketing, we focus on target accounts that fit our ICP (ideal customer profile). 

## Where does account based marketing fit within the greater marketing org?
Account based marketing sits next to field marketing and is similarly aligned to sales.  Account based marketing looks at an account or target account as a market of one, versus marketing to the total addressable market or to a specific region.  This type of marketing is executed by the account based team whereas field marketing is focused on lead gen and account centric marketing.

## What does account based marketing look like at GitLab and what is the team responsible for?
The account based marketing team is responsible for all account based marketing strategies, finding and targeting our ideal customer, and managing our ideal customer profile.  You can think of this team as sitting next to all other marketing functions and marketing to our highest value target accounts utilizing and building on the other marketing efforts already in motion.

## Roles & Responsibilities

**Emily Luehrs**  
*Manager, Account Based Marketing*
* **Prioritization** plans prioritization of the teams work on projects, campaign and strategy
* **Development**: plan account based marketing strategy, prioritize company objectives as it aligns with ABM
* **Strategy**: plan, prioritize and manage execution of campaigns
* **Ideal Customer Profile**: acts as project manager for the development of our ICP

**Megan O'Dowd**
*Account Based Marketing Manager*
* **Campaigns** executes on account based marketing campaigns globally
* **Strategy** develops and implements campaign strategy

## SDR<>ABM Alignment
As the account based team is focused on our high value accounts, it is natural to pair our acceleration team with the account based team to help accelerate the overall opportunity.  Beginning Q2 FY21, we will be pairing the acceleration team and account-based team in order to align our most senior SDR’s to our highest value accounts while also solving for the need to accelerate sales onboarding and warming up sales territories.  In parallel, we will also be incorporating the mapped SDR team in region into the plan and motions of the acceleration teams so as to encourage the transition to more acceleration based tactics across the entire organization, utilizing the intent data and other tools through the account based team. Detailed information about the ABM SDR workflow can be found on the [SDR handbook page](/handbook/marketing/revenue-marketing/sdr/#abm-acceleration-team). 

## Tools we use

#### Demandbase 
The Account Based Marketing Team owns Demandbase for GitLab.  It is a targeting and personalization platform which we use to target online ads to companies that fit our ICP and tiered account criteria.  Demandbase also has a wealth of intent data that is available to us through it's integration with Salesforce. For more information about this tool and how you can request leveraging it check out the [Demandbase handbook page](/handbook/marketing/revenue-marketing/account-based-marketing/demandbase) 

#### Pathfactory
The account based team utilizes Pathfactory to create customized experiences and guided content jounreys for ABM target accounts.  This includes content tracks and customize landing pages.  To learn more about Pathfactory you can head over to the [Pathfactory handbook page](/handbook/marketing/marketing-operations/pathfactory/)

#### TOPO
Research and advisory firm used by companies to develop and orchestrate their account based strategy.  We will be following their model for developing our ideal customer profile (ICP) and account based orchestration plays.
[TOPO research we are using](https://drive.google.com/drive/folders/1PC9Fqri-_JiJM1107B7k-ejD20gV3CnM?usp=sharing)

## Account Based Marketing workflow & labels in GitLab   

The ABM team works from issues and issue boards. If you are needing our assistance with any project, please open an issue and tag us using the ~Account Based Marketing label anywhere within the GitLab repo. 

The ABM team has a few different boards to manage campaigns and projects as well as our project:
##### [Global issue board](https://gitlab.com/groups/gitlab-com/-/boards/1409957) 
##### [Global campaign Board](https://gitlab.com/groups/gitlab-com/-/boards/1409957?&label_name[]=ABM%20campaign)
##### [Account Based Marketing Project](https://gitlab.com/gitlab-com/marketing/account-based-marketing)

#### How we manage our workflow
The ABM team uses a combination of labels and milestones to manage our work.  In general our labels show the type of work an issue covers, and the milestone show when the work is planned for.

##### Milestone naming convention: ab_strategy: 20200831-20200906

Global labels used by the team:  
- `Account Based Marketing`: pulls the issue into the board, and is used to put an issue on the team's radar
- `ABM FYI`: used to put something on the account based marketing team's radar, however they are not the DRI not is any action needed at this time (example: and account centric campaign being run by field marketing)
- `ABM Campaign`: Used to identify account based marketing campaigns
- `ABM::blocked`: issues/work that is currently blocked.  The team may be waiting in additional information or could be planning to execute but at a later date due to other circumstances that will be noted in the issue
- `ABM tier 1`: issues and epics related to tier 1 campaigns
- `ABM tier 2`: issues and epics related to tier 2 campaigns
- `ABM tier 3`: issues and epics related to tier 3 campaigns
- `ABM Nomination`: tracks all abm account nominations

## Ideal Customer Profile
The account based marketing team is responsible for developing and managing our ideal customer profile.  An ideal customer profile is the description of our "perfect" customer company (not individual or end user).  The profile takes into consideration firmographic, environmental and additional factors to develop our target list of highest value accounts. 

Gitlab first developed our ideal customer profile in FY21 Q1 to help focus our efforts on the highest value accounts.  Below is the profile we set.  One thing to note about our ICP is that it is fairly broad, mainly because GitLab can ultimately sell to a vast number of companies versus say, a banking solution that would have a much smaller TAM (total addressable market).  Because of this, we do not target ALL the accounts that fit our ICP at a given time, but rather, focus on a subset based on different variables including propensity to buy and additional intent data.

|  | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of developers (using company size as proxy) | 2,000+ |
| | Tech stack | Includes GitHub, Perforce, Jenkins, BitBucket or Cloudbees |
| | Titles that exist within the company | Includes Application development manager/director/executive & CISO  |
| | Prospect/Customer | Not a current PAID customer for GitLab  |
| **Additional criteria (attributes to further define)** | Annual technology spend | 4% of annual revenue | 
| | High intent account | Account is trending as high intent based on our data in Demandbase |
| | New hire | VP of IT, DevOps leadership role |

## NOTE:  Our account based strategy is in the process of being revamped in conjunction with Sales and Marketing alignment on GTM strategy

### Tier 1 (1:1 or strategic) - ICP accounts
**Our highest value target accounts.  Will include 5-10 accounts at any given time globally. Will receive customized account marketing plan w personalized content**
1.  be a 100% match to our ideal customer profile + additional criteria based on our focus at the time (i.e. a certain vertical, etc)
2.  be showing `HIGH` intent signals
3.  top account for sales
4.  have an engaged sales team ready to enact a fully customized marketing plan
      - have a completed marketing plan developed through collaboration of the ABM and Sales team for that account

### Tier 2 (1:few or scale)-Like accounts based on a certain number of target account qualifiers
**Will include roughly 50 accounts at any given time globally.  These accounts will receive an orchestration play that is a medium lift and focused on like accounts.  This mean there may be three different campaigns running in the segment, focused on a set of accounts rather than customized to a single account.**
1.  account fitting our ideal customer profile (ICP)
2.  must be showing `HIGH` intent (based on Demandbase data)
3.  high priority for sales but may have a lower lifetime value for GitLab
4.  falls into the banking, financial services, and transportation verticals

### Tier 3 (1:many or programmatic)- Accounts we would like to target but without the personalize plan or resources
**Will include 75-100 accounts globally at any given time. These accounts will have a digital campaign based on use case along with light email and SDR support**
1.  fits our ICP criteria AND
2.  showing `HIGH` intent signals in Demandbase OR
3.  is a focus for sales in the territory

## When does an account get added to our account based marketing strategy or move tiers?
The account based marketing team will be monitoring accounts in all tiers and adjusting the level of marketing support for these accounts based on the accounts tiering qualifications.  For an example, if an account is currently in tier 3 and then meets all of tier 2 qualifications, that account will be moved to tier 2.  Additionally, the account team will be notified and brought in to strategize and engage in the marketing plan appropriately.

Accounts can also move tiers by being nominated by sales and accepted by the account based team.  This is done through completing this [issue template](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/issues/new#) and assigning it to @emilyluehrs.  The account will be evaluated by the account based team which has an SLA for a decision within 5 business days of the issue being submitted.

## Other times when accounts will move
As our ICP iterates (we will be reviewing in July 2020 and then annually after that) we will be moving accounts into the account based strategy and could also be removing accounts or moving them to a lower tier.  This could be because the intent signals have dropped to a level that does not support a custom marketing plan, or the sales team is not supporting the marketing plan as needed.  An explanation will always be given if an account is moved to a different tier or removed from our account based strategy altogether.

## Accounts are identified in Salesforce by the GTM strategy field in Salesforce:

**VOLUME**
Default selection for all accounts.

**ACCOUNT CENTRIC**
Indicates that an account is a focus for field marketing and account centric campaigns.

**ACCOUNT BASED**
This defines that an account is included in one of the three tiers of our account based strategy.  If an account is identified with this strategy, the additional field of `ABM Tier ` that will be completed as well which will identify which tier the account falls into in respect to our account based strategy.

**ABM Tier**
This field is a sub field of GTM Strategy and will be populated if `Account Based` is chosen in the `GTM Strategy` field.  This field will identify which tier an account is currently in.

## Prepping an account for account based marketing
If an account is selected for an ABM campaign there are some clean up/prep todo's that need to happen before the account can be added.  This is done in the ABM nomination issue OR as part of the data clean up by the account owner prior to the start of the campaign.

#### Salesforce Fields

* [ ]  Country
* [ ]  Industry
* [ ]  GTM Strategy
* [ ]  ABM Tier
* [ ]  Domain
* [ ]  SDR assigned (updated to ABM campaign SDR)
* [ ]  Account Owner (SAL)
* [ ]  Technology
* [ ]  Website

#### Additional clean up
* [ ]  account hierarchy is correct and all child accounts roll up into the correct parent account

## What resources and marketing do each of the tiers receive?
Each campaign for ABM is built similarly to an integrated marketing campaign.

**`Tier 1`** account campaigns are built specific to that account i.e. specifc content and content journey to that account

**`Tier 2`** account campaigns are semi-customized based on firmographic or use case campaign plans.  An example would be a campaign specific to financial service companies that fit our ideal customer profile, or a campaign built around a set of accounts that are exhibiting `HIGH` intent signals and have a use case in common.

**`Tier 3`** programatic campaign that is less resource heavy and a lighter lift to develop and execute than tier 2 or tier 1.  This is more of a "one size fits most" tactic, relying on the account team to customize outreach as needed.

