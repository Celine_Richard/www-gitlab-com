---
layout: markdown_page
title: "GitLab vs GitHub Support Comparison"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->


## On this page
{:.no_toc}

- TOC
{:toc}

## GitHub Support Options

![GitLab Support Summary Config Chart](/devops-tools/github/images/github-support-summary.png)

## Community & System Status Support

**GitLab vs GitHub**

|                             |    GitLab    |    GitHub    |
|-----------------------------|:------------:|:------------:|
|             License/Product | Core & Above | Free & Above |
|           Community Founded |     2015     |     2008     |
|                       Users |   Millions   |  40 Million  |
|           Code Contributors |    3,000 +   |       0      |
|           Languages Support |      55      |    English   |
|         Meetup Contributors |    10,000    |    22,700    |
|    System Status Monitoring |      Yes     |      Yes     |
| System Status Notifications |      Yes     |      Yes     |

**Points to Understand when Considering GitLab vs GitHub Community Support**

![GitLab GitHub Community Questions Chart](/devops-tools/github/images/GitHub-Community-Questions.png)

## Standard Support

**GitLab vs GitHub**

|                                              |               GitLab              |               GitHub               |
|----------------------------------------------|:---------------------------------:|:----------------------------------:|
|                              License/Product |       Starter/Bronze & Above      |            Team & Above            |
|                        Communication Channel |      Email/Callback Possible      |                Email               |
|                            Next Business Day |                Yes                |                 No                 |
| Languages - Ticket and <br><br>Phone Support | Phone: English / <br><br>Ticket:7 | Phone: No / <br><br>Ticket:English |
|                                        Hours |               *24x5               |                24x5                |
|                                  Deployments |        Self Managed & .com        |                .com                |

_GitLab Support Hours: 3 pm PST-Friday 5pm PST_ <br>
_GitLab support only provides outbound/callbacks; we don’t support  in-bound calls_

**Points to Understand when Considering GitLab vs GitHub Standard Support**

![GitLab GitHub Standard Questions Chart](/devops-tools/github/images/GitHub-Standard-Questions.png)

## GitLab Priority vs. GitHub Enterprise Support

**GitLab vs GitHub**

|                                    |                     GitLab                     |            GitHub            |
|------------------------------------|:----------------------------------------------:|:----------------------------:|
|                    License/Product |         Premium/Ultimate & Silver/Gold         |   Enterprise Cloud & Server  |
|              Communication Channel |             Email/Callback Possible            |             Email            |
|   Response Time by Ticket Priority | 4 Priorities:<br><br>*30 min, 4hrs, 8hr, 24hrs |  8 Hours Response Time Only  |
|                              Hours | 24x7 (*Emergency),24x5 (Others)                | Mon-Friday (Local Time Zone) |
| Languages-Ticket Phone and Support | Phone: English / Ticket: 7                     | Ticket: English and Japanese |
|                  Emergency Support | Yes (Self Managed Only)                        | No                           |
|            Live Upgrade Assistance | Yes                                            | No                           |
|               Technical/Support AM | Yes                                            | No                           |

_*Self Managed Only_ <br>

**Points to Understand when Considering GitLab Priortiy vs GitHub Enterprise Support**

![GitLab GitHub Enterprise Questions Chart](/devops-tools/github/images/GitHub-EnterpriseSupport-Questions.png)

## GitLab Priority vs. GitHub Premium Support

**GitLab vs GitHub**


|                                    |                     GitLab                     |                GitHub                |
|------------------------------------|:----------------------------------------------:|:------------------------------------:|
|                    License/Product |         Premium/Ultimate & Silver/Gold         |       Enterprise Cloud & Server      |
|              Communication Channel |             Email/Callback Possible            |            Email and Phone           |
|   Response Time by Ticket Priority | 4 Priorities:<br><br>*30 min, 4hrs, 8hr, 24hrs | 4 Priorities:<br>*30 min, 4hrs, ?, ? |
|                              Hours |         24x7 (*Emergency),24x5 (Others)        |                 24x7                 |
| Languages-Ticket Phone and Support |           Phone: English / Ticket: 7           |   Phone: English / Ticket: English   |
|                  Emergency Support |             Yes (Self Managed Only)            |                  Yes                 |
|            Live Upgrade Assistance |                       Yes                      |                  Yes                 |
|               Technical/Support AM |                       Yes                      |                  Yes                 |

_*Self Managed Only_ <br>

**Points to Understand when Considering GitLab Priortiy vs GitHub Premium Support**

![GitLab GitHub Premium Questions Chart](/devops-tools/github/images/GitHub-Premium-Questions.png)

## GitLab Priority vs. GitHub Premium Plus Support

**GitLab vs GitHub**

|                                      |                 GitLab                 |                 GitHub                 |
|--------------------------------------|:--------------------------------------:|:--------------------------------------:|
|                      License/Product |     Premium/Ultimate & Silver/Gold     |        GitHub One Cloud & Server       |
|                Communication Channel |         Email/Callback Possible        |             Email and Phone            |
|     Response Time by Ticket Priority | 4 Priorities:*30 min, 4hrs, 8hr, 24hrs |   **4 Priorities:30 min, 4 hrs, ?, ?   |
| Hours                                |     24x7 (*Emergency),24x5 (Others)    | **24 x 7 (Except for certain features) |
| Languages - Ticket Support and Phone |                    7                   |                 English                |
| Emergency Support                    |         Yes (Self Managed Only)        |   **Yes (Except for certain features)  |
| Live Upgrade Assistance              |                   Yes                  |                   Yes                  |
| Technical/Support AM                 |                   Yes                  |                   Yes                  |
| Managed Services                     |                  None                  |              4 Hours/month             |


**Points to Understand when Considering GitLab Priortiy vs GitHub Premium Plus Support**

![GitLab GitHub Premium Plus Questions Chart](/devops-tools/github/images/GitHub-PremiumPlus-Questions.png)

## GitHub + Visual Studio Support

**GitHub Enterprise user that have Visual Studio subscriptions are provided access to Azure Support resources.  Here are the support options:**

- Azure Community: Visual Studio Enterprise and Professional (monthly cloud)
- Concierge Support (chat): Visual Studio Enterprise and Professional (monthly cloud) 
    - Support available 24/7 in English only
- Technical Support: Visual Studio Enterprise (4 incidents) and Professional (2 incidents)
    - Support for non-production environment only
        <br>

**Points to Understand when Considering GitHub + Visual Studio Support:**

![GitLab GitHub Visual Studio Questions Chart](/devops-tools/github/images/GitHub-VisualStudio-Questions.png)
