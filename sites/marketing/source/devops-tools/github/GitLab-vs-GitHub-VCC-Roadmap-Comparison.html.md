---
layout: markdown_page
title: "GitLab vs GitHub Version Control & Collaboration Roadmap Comparison"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->


# Background

On July 28, 2020 [GitHub announced their roadmap will be made public](https://github.blog/2020-07-28-announcing-the-github-public-roadmap/).  GitLab welcomes this move by GitHub since it ensures greater transparency and enables customers to more easily compare the product direction and potential benefits to their DevOps processes.  

GitLab has provided VC&C use case since its inception.  Likewise, GitHub also traces its DNA and strengths to this use case.

This page highlights two aspects:

-  Compares the VC&C capability as documented in GitHub roadmap with similar GitLab capability.  It also shows dates when those capabilities were available or planned for availability.
-  Highlights select additional capability present in GitLab, that is not yet reflected in the GitHub's roadmap.

For the full details on GitLab roadmap please visit the [GitLab Directions](/direction/) page.  GitLab also measures its overall maturity against various DevOps Stages.  This self-assessment can be found here in the [Product Maturity](/direction/maturity/) page.


# Version Control and Collaboration Roadmap Comparison


![Roadmap Comparison](/devops-tools/github/images/vcc-roadmap.png)
