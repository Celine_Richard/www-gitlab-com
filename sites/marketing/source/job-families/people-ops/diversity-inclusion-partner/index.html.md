---
layout: job_family_page
title: "Diversity, Inclusion and Belonging"
---
The Diversity, Inclusion and Belonging team will build an environment where all team members feel they belong: they are recognized, allowed to express their thoughts, have the ability to make contributions and are able to bring their best selves to work every day. As GitLab continues to grow, we look for people from a wide range of backgrounds to reflect the global nature of our customers and we want to have the tools and resources available to support that growth. It takes diversity of thought, culture, background, and perspective to build a truly global company. The DIB team members will build and work with external partnersnips as well as closely with internal departments such as recruiting, Marketing, Communications, Talent Brand, etc to help shape the overall DIB strategy that creates, supports and fuels GitLab’s growth.


## Diversity, Inclusion, and Belonging Specialist
The Diversity, Inclusion and Belonging Specialist will report to the DIB Partner.  The Specialist will provide overall programatic and strategy support to the DIB team.

### Responsibilities
- Deliver insights and coordinate DIB efforts across multiple functions 
- DIB monthly call action item follow up and communication of status 
- Proactive support of all DIB projects throughout the organization
- Develop content and tracks for internal / external events (virtual and in-person)
- Ensure DIB boards are moving along, labeled, organized, issues closed out as needed, monitor progress 
- Primary responder to DIB email, Slack channel to ensure response to team members at large here at GitLab
- Provide support to TMRGs to ensure effectiveness, compliance and overall alignment to company goals
- Monitor training requests, update content, build new content
- Monitor and approve sponsorship request via the GitLab tool as in accordance with criteria for approval
- Support communication of DIB efforts across GitLab
- Provide support to DIB Partner for liasions with external and internal PR teams including content creation "
- Monitor and communicate the status of metric request

### Requirements
- 1+ years of experience in human resources or related business experience
- 1+ years of experience leading diversity projects, diversity sourcing initiatives, and/or recruiting initiatives
- Foundational understanding of Diversity, Inclusion and Belonging 
- Strong business acumen and ability to connect DIB learning to business needs
- Ability to partner with leadership and team members
- Skilled at taking a consultative and collaborative approach to problem solving
- Experience driving change management initiatives successfully and flexing with shifting priorities
- Strong communication skills
- Strong project management and analytical skills
- Proven passion for DIB
- Excellent EQ and an empathetic communication style that fosters connection, collaboration, and confidence
- Strong ability to plan proactively and react quickly when problems arise
- Confidence to rely on own judgment and discretion to work independently, keep multiple plates spinning, and take smart risks
- Experience working in a global environment preferred
- You share our [values](/handbook/values), and work in accordance with those values
- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)
- Ability to use GitLab

### Job Grade
The Diversity, Inclusion and Belonging Specialist is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Performance Indicators
- [Engagement survey inclusion score](/company/culture/inclusion/#performance-indicators)

### Career Ladder
The next step in the Diversity, Inclusion, and Belonging Specialist is to move to into DIB Partner role 

## Hiring Process
- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters
- Next, the candidate will be invited to interview with the DIB Manager
- Next, the candidate will be invited to interview with a member of the Learning and Development team and a People Business Partner

## Diversity, Inclusion, and Belonging Partner
The Diversity, Inclusion and Belonging Partner will help build an environment where all team members feel they belong and can show up as their full selves by collaborating with all departments within GitLab as well as external.  They will also work closely as needed with the CEO and E-group and guided under the leadership of the DIB Manager.

## Responsibilities
- Partner with the People Operations team, organizational leaders, and People Business Partners to continue building out our diversity, inclusion and belonging strategy
- Consult with organizational leaders to provide mentorship and coaching on how to achieve results
- Works with the Recruiting team to create and implement a diversity recruitment strategy
- Develop and improve relationships with current and new diversity organizations and partnerships
- Increase organizational capabilities in cross-cultural intelligence, mitigating unconscious bias, and inclusive leadership
- Provide communication and visibility of DIB efforts across GitLab
- Design leadership development programs focused on underrepresented groups
- Review and analyze data to monitor progress
- Conduct research and benchmarking on DIB standard methodologies and emerging trends and apply that to relevant programs aligned to business needs
- Collaborate strategically to engage team members, amplify successes, and ensure inclusive internal and external messaging
- Build out a library of diversity, inclusion and belonging materials

## Requirements
- 5+ years of experience in human resources or related business experience
- 3+ years of experience leading diversity projects, diversity sourcing initiatives, and/or recruiting initiatives
- 3+ years leadership experience, preferably in the DIB space
- Strong business acumen and ability to connect DIB learning to business needs
- Deep understanding of diversity, inclusion and belonging principles and practices
- Ability to influence leadership and enroll employee support and engagement
- Skilled at taking a consultative and collaborative approach to problem solving
- Experience driving change management initiatives successfully and flexing with shifting priorities
- Experienced facilitation and communication skills
- Strong project management and analytical skills
- Proven passion for inclusion and diversity
- Excellent EQ and an empathetic communication style that fosters connection, collaboration, and confidence
- Strong ability to plan proactively and react quickly when problems arise
- Confidence to rely on own judgment and discretion to work independently, keep multiple plates spinning, and take smart risks
- Experience working in a global environment preferred
- You share our [values](/handbook/values), and work in accordance with those values
- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)
- Ability to use GitLab


### Job Grade 
The Diversity, Inclusion, and Belonging Partner is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Performance Indicators
- [Women globally as a whole at GitLab](/company/culture/inclusion/#performance-indicators)
- [Women in leadership](/company/culture/inclusion/#performance-indicators)
- [Women voluntary attrition](/company/culture/inclusion/#performance-indicators)
- [Pay equality](/company/culture/inclusion/#performance-indicators)
- [Engagement survey inclusion score](/company/culture/inclusion/#performance-indicators)

### Career Ladder
The next step in the Diversity, Inclusion, and Belonging job family is to move to a management role which is not yet defined at GitLab. 

## Hiring Process
- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute panel interview with one of our Senior Recruiters and our Diversity Sourcer
- Next, candidates will be invited to schedule a 45 minute interview with our Recruiting Director
- After that, candidates will be invited to schedule a 30 minute interview with our Chief People Officer
- Next, the candidate will be invited to interview with a People Business Partner
- After that, the candidate will be invited to interview with our CFO or another member of our executive team
- Finally, our CEO may choose to conduct a final interview


