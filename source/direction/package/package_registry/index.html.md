---
layout: markdown_page
title: "Category Direction - Package Registry"
description: "As part of our overall vision for packaging at GitLab, we want to provide a single interface for managing dependencies, registries, and package repositories."
canonical_path: "/direction/package/package_registry/"
---

- TOC
{:toc}

## Package Registry

Our goal is for you to rely on GitLab as a universal package manager, so that you can reduce costs and drive operational efficiencies. The backbone of this category is your ability to easily publish and install packages, no matter where they are hosted. 

You can view the list of supported and planned formats in our documentation [here](https://docs.gitlab.com/ee/user/packages/).

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3APackage+Registry)
- [Overall Vision](https://about.gitlab.com/direction/ops/#package)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## What's Next & Why

We are currently focused on two epics for the GitLab Package Registry.

First, we'll improve our reporting and accuracy of how much storage is being consumed by the Package Registry. The epic [gitlab-#1422](https://gitlab.com/groups/gitlab-org/-/epics/1422) details our plan to achieve this. 

The second, [gitlab-#2867](https://gitlab.com/groups/gitlab-org/-/epics/2867) will move the Package Registry to GitLab Core, so you can rely on GitLab for all of your Package management needs, regardless of your pricing tier.

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is "Complete" (see our [definitions of maturity levels](/direction/maturity/)).

For a list of key deliverables and expected outcomes, check out the epic, [gitlab-#2891](https://gitlab.com/groups/gitlab-org/-/epics/2891), which includes links and expected timing for each issue.

## Competitive Landscape

### Universal package management tools
Artifactory and Nexus are the two leading univeral package manager applications on the market. They both offer products that support the most common formats and additional security and compliance features. A critical gap between those two products and GitLab's Package offering is the ability to easily connect to and group external, remote registries. To date, GitLab has been focused on delivering Project and Group-level private package registries for the most commonly used formats. We plan on bridging this gap by expanding the Dependency Proxy to support remote and virtual registries. 

### Cloud providers
Azure and AWS both offer support for hosted and remote registries for a limited amount of formats. 

### GitHub
GitHub offers a package management solution as well. They offer project-level package registries for a variety of formats. On their roadmap for Q4 2020 and Q1 2021 they will add support for GitHub Enterprise and GitHub private instances. They have implementations with Composer and PyPI planned but not scheduled. They charge for storage and network transfers. GitHub does a nice job with search and reporting usage data on how many times a given package has been downloaded. They do not have anything on their roadmap about supporting remote and virtual registries, which would allow them to group registries behind a single URL and allow them to act as a universal package manager, like Artifactory or Nexus or GitLab.

### Supported formats

The below table lists our supported and most frequently requested package manager formats. Artifactory and Nexus both support a longer list of formats, but we have not heard many requests from our customers for these formats. If you'd like to suggest we consider a new format, please open an issue [here](https://gitlab.com/gitlab-org/gitlab/-/issues). 

|         | GitLab | Artifactory | Nexus | GitHub | Azure Artifacts | AWS CodeArtifact |
| ------- | ------ | ----------- | ----- | ------ | ------ | ------ |
| Composer    | ✔️ | ✔️ | ✔️️️️ | - | - | - |
| Conan       | ✔️ | ✔️ | ☑️ | - | - | - |
| Debian      | - | ✔️ | ✔️ | - | - | - |
| Maven       | ✔️ | ✔️ | ✔️ | ️✔️ ️| ✔️ | ✔️ |
| NPM         | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ |
| NuGet       | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | - |
| PyPI        | ✔️ | ✔️ | ✔️ | - | ✔️ | ✔️ |
| RPM         | - | ✔️ | ✔️ | - | - | - |
| RubyGems    | - | ✔️ | ✔️ | ✔️ | - | - |


☑️ _indicates support is through community plugin or beta feature_

Interested in contributing a new format? Please check out our [suggested contributions](https://docs.gitlab.com/ee/user/packages/#suggested-contributions).

## Top Customer Success/Sales Issue(s)

- Our top customer success and sales issues are all focused on adding support for new package manager formats:
  - [gitlab-#803](https://gitlab.com/gitlab-org/gitlab/issues/803): RubyGems
  - [gitlab-#5835](https://gitlab.com/gitlab-org/gitlab/issues/5835): Debian
  - [gitlab-#5932](https://gitlab.com/gitlab-org/gitlab/issues/5932): RPM

## Top Customer Issue(s)

- [gitlab-#212209](https://gitlab.com/gitlab-org/gitlab/-/issues/212209) which resolve issues with downlading NuGet packages and dependencies. 


## Top Internal Customer Issue(s)

- The Distribution team utilizes an external tool for building Linux packages. By offering support for Debian and RPM we can begin to remove that external dependency.
- The GitLab Distribution team also utilizes RubyGems for downloading external dependencies. They would like to speed up their build times and remove their reliance on external dependencies by caching frequently used packages. This would require an integration with RubyGems as well as the dependency proxy. [gitlab-#225](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/225) details their needs and requirements.
- The Release group would like to leverage the GitLab Package Registry to store release assets and make them available for discovery and download. Although we are still working through how to best implement this, [gitlab-#36133](https://gitlab.com/gitlab-org/gitlab/issues/36133) discusses the need and use cases.

## Top Vision Item(s)

[gitlab-#2614](https://gitlab.com/groups/gitlab-org/-/epics/2614), captures our plan to improve our existing integrations by expanding our existing product to include the creation, management and usage of **remote** and **virtual** repositories.  

